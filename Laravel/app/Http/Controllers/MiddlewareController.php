<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiddlewareController extends Controller
{
    public function super()
    {
        return 'ini halaman super admin';
    }

    public function admin()
    {
        return 'ini halaman admin';
    }

    public function guest()
    {
        return 'ini halaman guest';
    }
}
