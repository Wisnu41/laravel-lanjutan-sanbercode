<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RouteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $params = 2)
    {
        $userRole = Auth::user()->role;
        
        // 0 untuk super admin
        // 1 untuk admin
        // 2 untuk guest

        if($userRole <= $params){
            return $next($request);
        }

        return abort(403);
    }
}
