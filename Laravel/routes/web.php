<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware('auth')->group(function()
{
    Route::get('/super-admin','MiddlewareController@super')->middleware('role:0');
    Route::get('/admin','MiddlewareController@admin')->middleware('role:1');
    Route::get('/guest','MiddlewareController@guest')->middleware('role:2');
});