<?php
  trait Fight{
    public $attakPower;
    public $defencePower;

    public function serang($penyerang,$diserang)
    {
      echo $penyerang->nama . ' sedang menyerang ' . $diserang->nama . '<br />';

      $diserang->diserang($penyerang, $diserang);
    }

    public function diserang($penyerang,$diserang)
    {
      echo $diserang->nama . ' sedang diserang ' . $penyerang->nama . '<br />';

      $diserang->darah = $diserang->darah - $penyerang->attackPower / $diserang->defencePower;
    }
  }

  trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
      echo $this->nama . ' sedang ' . $this->keahlian . '<br />';
    }
  }

  class Elang{
    use Fight;
    use Hewan;

    function __construct($nama){
      $this->nama = $nama;
      $this->jumlahKaki = 2;
      $this->keahlian = "terbang tinggi";
      $this->attackPower = 10;
      $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
      echo 'nama :'.$this->nama . '<br />';
      echo 'jumlahKaki :'.$this->jumlahKaki . '<br />';
      echo 'keahlian :'.$this->keahlian . '<br />';
      echo 'darah :'.$this->darah . '<br />';
      echo 'attakPower :'.$this->attackPower . '<br />';
      echo 'defencePower :'.$this->defencePower . '<br />';
      echo 'jenisHewan : Elang' . '<br />';
    }
  }

  class Harimau{
    use Fight;
    use Hewan;

    function __construct($nama){
      $this->nama = $nama;
      $this->jumlahKaki = 4;
      $this->keahlian = "lari cepat";
      $this->attackPower = 7;
      $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
      echo 'nama :' . $this->nama . '<br />';
      echo 'jumlahKaki :' . $this->jumlahKaki . '<br />';
      echo 'keahlian :' . $this->keahlian . '<br />';
      echo 'darah :' . $this->darah . '<br />';
      echo 'attakPower :' . $this->attackPower . '<br />';
      echo 'defencePower :' . $this->defencePower . '<br />';
      echo 'jenisHewan : Harimau' . '<br />';
    }
  }

  $elang1 = new Elang('Elang 1');
  $harimau1 = new Harimau('Harimau 1');

  
  $harimau1->serang($harimau1,$elang1);
  
  $elang1->getInfoHewan();